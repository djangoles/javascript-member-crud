//=================================================
// -----------------MEMBER CONTROLLER--------------
//=================================================

var memberController = (function(){
  // MEMBER MODEL
  var Member = function(id, name, email, memberLevel) {
    this.id = id;
    this.name = name;
    this.email = email;
    this.memberLevel = memberLevel;
  }
  // MEMBER STORE
  var data = {
    allMembers: []
  };

  // PUBLIC METHODS---------------------------
  return {

    // CREATES NEW MEMBER
    addItem: function(name, email, memberLevel) {
      var newItem, ID;
      if(data.allMembers.length > 0) {
        ID = data.allMembers[data.allMembers.length - 1].id + 1;
      } else {
        ID = 0;
      }
      newItem = new Member(ID, name, email, memberLevel);
      data.allMembers.push(newItem);
      return newItem;
    },
    // GETS SINGLE MEMBER
    getmember: function(id) {
      var memberObj, ids, index;
      ids = data.allMembers.map(function(current) {
        return current.id;
     });

     index = ids.indexOf(id);
     if(index > -1) {
       return data.allMembers[index];
     }
    },
    // UPDATE SINGLE MEMBER
    updateMember: function(id, obj) {
      var ids, index;
      ids = data.allMembers.map(function(current) {
        return current.id;
      });
      index = ids.indexOf(id);
      if(index > -1) {
        data.allMembers[index].name = obj.name; 
        data.allMembers[index].email = obj.email;  
        data.allMembers[index].memberLevel = obj.membership;
        return data.allMembers[index];
      }
    },
    // DELETE SINGLE MEMBER
    deleteSingleMember: function(id) {
      var ids, index;
      ids = data.allMembers.map(function(current) {
        return current.id;
      });
      index = ids.indexOf(id);
      if(index !== -1) {
        data.allMembers.splice(index, 1);
      }
    },

    testing: function(){
      console.log(data);
    }
  };
})();

//=================================================
// -----------------UI CONTROLLER------------------
//=================================================

var UIController = (function(){
  //STORE CSS CLASSES
  var DOMstrings = {
    showInputBtn: 'show-input',
    submitBtn: 'submit-input',
    updateBtn: 'submit-update',
    addItem: '.add-item',
    formWrap: '.form-wrap',
    formWrapShow:  '.form-wrap-show',
    hideInput: 'cancel-input',
    inputName: 'input-name',
    inputEmail: 'input-email',
    inputMembership: 'input-membership',
    memberContainer: '.member-container',
    memberItemContainer: '.member-item-container',
    singleMemberItem: '.single-member-item',
    singleMemberShow: '.single-member-show',
    singleContainer: 'single-container'
  };

  var checkMemberLevel = function(level) {
    var imgSrc;
    if(level === 'free') {
      imgSrc = "./free.png";
    } else if(level === 'basic') {
      imgSrc = "./basic.png";
    } else {
      imgSrc = "./prem.png";
    }

    return imgSrc;
  };

  // PUBLIC METHODS---------------------------
  return {

    showForm: function() {
    document.querySelector(DOMstrings.addItem).style.display = "none";
    var form = document.querySelector(DOMstrings.formWrap);
    form.classList.add("form-wrap-show");
    },

    hideForm: function() {
      var form = document.querySelector(DOMstrings.formWrap);
      form.classList.remove("form-wrap-show");
      document.getElementById(DOMstrings.submitBtn).style.display = "inline-block";
      document.getElementById(DOMstrings.updateBtn).style.display = "none";
      setTimeout(function(){
        document.querySelector(DOMstrings.addItem).style.display = "block";
      },125)
    },

    getInputForm: function() {
      return {
        name: document.getElementById(DOMstrings.inputName).value,
        email: document.getElementById(DOMstrings.inputEmail).value,
        membership: document.getElementById(DOMstrings.inputMembership).value,
      }
    },

    addListItem: function(obj) {
      var html, newHtml, element;

      element = DOMstrings.memberContainer;
      html = `<div id="member-%id%" class="member-item-container">
                <p>Name: %name%</p>
                <button data-btn="view" class="btn btn-success btn-sm btn-view">View</button>
                <button data-btn="edit" class="btn btn-primary btn-sm">Edit</button>
                <button data-btn="delete" class="btn btn-danger btn-sm">Delete</button>
                <hr>
              </div>`;

      newHtml = html.replace('%id%',obj.id);
      newHtml = newHtml.replace('%name%', obj.name);
      document.querySelector(element).insertAdjacentHTML('beforeend', newHtml);
    },

    viewSingle: function(obj) {
      var html, newHtml, element, membershipLevel; 
      membershipLevel = obj.memberLevel;
      //RETURNS img src based on Membership Level
      var checkedLevel = checkMemberLevel(membershipLevel);
      element = DOMstrings.singleMemberItem;
      var singleItemContainer = document.querySelector(DOMstrings.singleMemberItem);
      singleItemContainer.dataset.viewupdate = obj.id;
      if(singleItemContainer.hasChildNodes()) {
        singleItemContainer.removeChild(singleItemContainer.childNodes[0]);
      }

        html = `<div data-member=%id% class="single-member-show">
                  <img class="member-img" src="%level%" alt="">
                  <h4>Name: %name%</h4>
                  <h4>Email: %email%</h4>
                  <h4>Membership: %membership%</h4>
                </div>`;

        newHtml = html.replace('%id%', obj.id);
        newHtml = newHtml.replace('%level%', checkedLevel);
        newHtml = newHtml.replace('%name%', obj.name);
        newHtml = newHtml.replace('%email%', obj.email);
        newHtml = newHtml.replace('%membership%', obj.memberLevel);
        document.querySelector(element).insertAdjacentHTML('beforeend', newHtml);
    },

    fillForm:function(obj) {
      //INSERT VALUE FORM

      document.getElementById(DOMstrings.inputName).value = obj.name;
      document.getElementById(DOMstrings.inputEmail).value = obj.email;
      document.getElementById(DOMstrings.inputMembership).value = obj.memberLevel;  
      //HIDE SUBMIT
      document.getElementById(DOMstrings.submitBtn).style.display = "none";
      var updateBtn = document.getElementById(DOMstrings.updateBtn);
  
      //SHOW SAVE
      updateBtn.style.display = "inline-block";  
      updateBtn.dataset.update = obj.id;    
    },

    updateMemberList:function(id, obj) {
      var singleContainer, singleContainerID, viewSingleItem, updateDiv;
      //WRAP DIV ALREADY ON PAGE
      singleContainer = document.querySelector(DOMstrings.singleMemberItem);
 
      // DATA ID FROM WRAP DIV 
      singleContainerID = parseInt(singleContainer.dataset.viewupdate);
      //THIS WILL BE NULL UNTIL ITEM IS VIEWED
      viewSingleItem = document.querySelector(DOMstrings.singleMemberShow);

      updateDiv = document.getElementById('member-' + id);

      updateDiv.innerHTML = `<p>Name: ${obj.name} </p>
                              <button data-btn="view" class="btn btn-success btn-sm btn-view">View</button>
                              <button data-btn="edit" class="btn btn-primary btn-sm">Edit</button>
                              <button data-btn="delete" class="btn btn-danger btn-sm">Delete</button>
                              <hr>`;
      if(singleContainerID > -1) {
        this.viewSingle(obj);
      } 
     },

    deleteSingleListItem: function(selectorID, id) {
      var viewSingleID;
      var viewSingleItem = document.querySelector(DOMstrings.singleMemberShow);

      if(id > -1) {
        viewSingleID = parseInt(viewSingleItem.dataset.member);
        console.log(selectorID);
        console.log(id);
        console.log(viewSingleID);
        var el = document.getElementById(selectorID);
        el.parentNode.removeChild(el);
        viewSingleItem.parentNode.removeChild(viewSingleItem);
      }

    },

    clearInputs: function() {
      document.getElementById(DOMstrings.inputName).value = "";
      document.getElementById(DOMstrings.inputEmail).value = "";
    },

    getDomStrings: function() {
      return DOMstrings;
    },

  };
})();



//=================================================
// -----------------CONTROLLER---------------------
//=================================================

var app = (function(memberCtl, UICtl) {

  var DOM = UICtl.getDomStrings();
  //LISTEN FOR CLICK OR ENTER KEYPRESS
  var setupEventListeners = function() {
    document.getElementById(DOM.showInputBtn).addEventListener('click', showInputForm);
    document.getElementById(DOM.hideInput).addEventListener('click', hideInputForm);
    document.getElementById(DOM.submitBtn).addEventListener('click', ctrlAddItem);
    document.querySelector(DOM.memberContainer).addEventListener('click', ctrlActionItem);
    document.getElementById(DOM.updateBtn).addEventListener('click', ctrlUpdateItem);
  };

  //Hide add button and show Input Form
  var showInputForm = function(e) {
    e.preventDefault(); 
    UICtl.clearInputs(); 
    UICtl.showForm();
  };

  var hideInputForm = function(e) {
    e.preventDefault();
    UICtl.hideForm();
  };

  var ctrlAddItem = function(e) {
    e.preventDefault();
    var input, newItem;
    input = UICtl.getInputForm();  
    if(input.name !== "" && input.email !== "" && input.membership !== "") {
      newItem = memberCtl.addItem(input.name, input.email, input.membership);
      UICtl.addListItem(newItem);
      UICtl.clearInputs();
      UICtl.hideForm();
    }
  };

  var ctrlActionItem = function(e) {
    e.preventDefault();
    var itemID, splitID, id, dataAttr;
    itemID = e.target.parentNode.id;
    itemClass = e.target.className;
    dataAttr = e.target.dataset.btn;

    //MOVE INTO FUNCTION------------------
    var splitID = itemID.split("-");
    splitID = parseInt(splitID.splice(1,1));
    //RETURNS THE ID
    var singleMember = memberCtl.getmember(splitID);
    //MOVE INTO FUNCTION------------------

    if(dataAttr === 'view') {
      UICtl.viewSingle(singleMember);

    } else if(dataAttr === 'edit') {
      UICtl.showForm();
      UICtl.fillForm(singleMember);
         
    } else if(dataAttr === 'delete') {
      memberCtl.deleteSingleMember(splitID);
      UICtl.deleteSingleListItem(itemID, splitID);
    }
  };

 var ctrlUpdateItem = function(e, id) {
    var dataID, singleMember, returnedMember;
    e.preventDefault();
    var updatedMember = UICtl.getInputForm(); 
    console.log(updatedMember);
    //RETURNS THE ID
    dataID = parseInt(document.getElementById(DOM.updateBtn).dataset.update);
    returnedMember = memberCtl.updateMember(dataID, updatedMember);
    UICtl.hideForm();
    UICtl.clearInputs();
    UICtl.updateMemberList(dataID, returnedMember); 

 };

  //PUBLIC EXPOSED RETURN OBJECT---------------------------------->>>>>
  return {
    init:function() {
      console.log('Application has started');
      setupEventListeners();
    }
  };
})(memberController, UIController);

app.init();


